﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    /// The following is a summary of things I will be keeping track of during this assignment
   /*
    1) Amount of mouse clicks
    2) Amount of Cards Clicked
    3) Every time a diamond is selected
    4) Play time per level
    5) Current Level Completed
    */



    int MouseClicksAmount;
    int DiamondsSelected;

    float levelTime = 0;
    int tilesselected;



    //The current level to be loaded
    public static int CurrentLevelNumber;
    public bool DiamondSelected = false;

    [System.Flags]
    public enum Symbols
    {
        Waves =             1 << 0,
        Dot =               1 << 1,
        Square =            1 << 2,
        LargeDiamond =      1 << 3,
        SmallDiamonds =     1 << 4,
        Command =           1 << 5,
        Bomb =              1 << 6,
        Sun =               1 << 7,
        Bones =             1 << 8,
        Drop =              1 << 9,
        Face =              1 << 10,
        Hand =              1 << 11,
        Flag =              1 << 12,
        Disk =              1 << 13,
        Candle =            1 << 14,
        Wheel =             1 << 15
    }

    //The description holds data such as rows and columns and all the symbols used in the level
    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    //The tile prefab is isntantiated here and the spacing is how far apart the cards will be
    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;
    //this loads the initial level

    private void Start()
    {

        LoadLevel( CurrentLevelNumber );

    }

    //When the level is loaded, its symbols and level description are assigned
    private void LoadLevel( int levelNumber )
    {
        levelTime = 0;
        print("LEVEL IS DONE");
        Analytics.CustomEvent("LevelReached", new Dictionary<string, object>
        {
            { "LevelCompleted",  levelNumber},
        });
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols( level );

        //The game objects for the cards are spawned and laid out evenly in rows and columns 
        for ( int rowIndex = 0; rowIndex < level.Rows; ++rowIndex )
        {
            float yPosition = rowIndex * ( 1 + TileSpacing );
            for ( int colIndex = 0; colIndex < level.Columns; ++colIndex )
            {
                float xPosition = colIndex * ( 1 + TileSpacing );
                GameObject tileObject = Instantiate( TilePrefab, new Vector3( xPosition, yPosition, 0 ), Quaternion.identity, this.transform );
                int symbolIndex = UnityEngine.Random.Range( 0, symbols.Count );
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol( symbols[symbolIndex] );
                tileObject.GetComponent<Tile>().Initialize( this, symbols[symbolIndex] );
                symbols.RemoveAt( symbolIndex );
            }
        }

        SetupCamera( level );
    }

    // The required symbols are the symbols that will be shown in each level
    private List<Symbols> GetRequiredSymbols( LevelDescription level )
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //All the symbols are loaded and they are parszed into pairs sop that the player can choose two and no more
        {
            Array allSymbols = Enum.GetValues( typeof( Symbols ) );

            m_cardsRemaining = cardTotal;

            if ( cardTotal % 2 > 0 )
            {
                new ArgumentException( "There must be an even number of cards" );
            }

            foreach ( Symbols symbol in allSymbols )
            {
                if ( ( level.UsedSymbols & symbol ) > 0 )
                {
                    symbols.Add( symbol );
                }
            }
        }

        //These are two quick checks that make sure the game works optimally
        {
            if ( symbols.Count == 0 )
            {
                new ArgumentException( "The level has no symbols set" );
            }
            if ( symbols.Count > cardTotal / 2 )
            {
                new ArgumentException( "There are too many symbols for the number of cards." );
            }
        }

        // If the cards repeat, this keeps track of them and makes sure they are still in pairs
        {
            int repeatCount = ( cardTotal / 2 ) - symbols.Count;
            if ( repeatCount > 0 )
            {
                List<Symbols> symbolsCopy = new List<Symbols>( symbols );
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for ( int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex )
                {
                    int randomIndex = UnityEngine.Random.Range( 0, symbolsCopy.Count );
                    duplicateSymbols.Add( symbolsCopy[randomIndex] );
                    symbolsCopy.RemoveAt( randomIndex );
                    if ( symbolsCopy.Count == 0 )
                    {
                        symbolsCopy.AddRange( symbols );
                    }
                }
                symbols.AddRange( duplicateSymbols );
            }
        }

        symbols.AddRange( symbols );

        return symbols;
    }

    // The offset helps keep track of the symbols and if it cannot find them, it breaks the sequence
    private Vector2 GetOffsetFromSymbol( Symbols symbol )
    {
        Array symbols = Enum.GetValues( typeof( Symbols ) );
        for ( int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex )
        {
            if ( ( Symbols )symbols.GetValue( symbolIndex ) == symbol )
            {
                return new Vector2( symbolIndex % 4, symbolIndex / 4 ) / 4f;
            }
        }
        Debug.Log( "Failed to find symbol" );
        return Vector2.zero;
    }

    // Puts the camera in the center of all the tiles
    private void SetupCamera( LevelDescription level )
    {
        Camera.main.orthographicSize = ( level.Rows + ( level.Rows + 1 ) * TileSpacing ) / 2;
        Camera.main.transform.position = new Vector3( ( level.Columns * ( 1 + TileSpacing ) ) / 2, ( level.Rows * ( 1 + TileSpacing ) ) / 2, -10 );
    }

    // If the tile is selected, the functions inside the Tile prefab are activated
    public void TileSelected( Tile tile )
    {
        if ( m_tileOne == null )
        {
            print("Tile Selected");
            tilesselected++;
            Analytics.CustomEvent("TileSelected", new Dictionary<string, object>
        {
               
            { "TilesSelected", tilesselected },
        });
            m_tileOne = tile;

            if (tile.Symbol == Symbols.LargeDiamond || tile.Symbol == Symbols.LargeDiamond)
            {
                DiamondsSelected++;
                print("DIAMOND " + DiamondsSelected);
                DiamondSelected = true;
                Analytics.CustomEvent("DiamondSelected", new Dictionary<string, object>
        {

            { "DiamondSelected", DiamondsSelected },
        });
            }
                m_tileOne.Reveal();
        }
        else if ( m_tileTwo == null )
        {
            print("Tile Selected");
            tilesselected++;
            Analytics.CustomEvent("TileSelected", new Dictionary<string, object>
        {

            { "TilesSelected", tilesselected },
        });
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if ( m_tileOne.Symbol == m_tileTwo.Symbol )
            {
                StartCoroutine( WaitForHide( true, 1f ) );
            }
            else
            {
                StartCoroutine( WaitForHide( false, 1f ) );
            }
        }
    }

    // If the level has no more cards, the new level is loaded
    private void LevelComplete()
    {
        print(levelTime);
        Analytics.CustomEvent("LevelTime", new Dictionary<string, object>
        {
            { "LevelTime", levelTime },
        });
        print(Time.time);
        tilesselected++;
        Analytics.CustomEvent("TotalTime", new Dictionary<string, object>
        {

            { "TilesSelected", Time.time },
        });


        ++CurrentLevelNumber;
        if ( CurrentLevelNumber > Levels.Length - 1 )
        {
            Debug.Log( "GameOver" );
        }
        else
        {
            SceneManager.LoadScene( SceneManager.GetActiveScene().buildIndex );
        }
    }

    // This stops all other functions while the tile is in transtition
    private IEnumerator WaitForHide( bool match, float time )
    {
        float timer = 0;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            if ( timer >= time )
            {
                break;
            }
            yield return null;
        }
        if ( match )
        {
            Destroy( m_tileOne.gameObject );
            Destroy( m_tileTwo.gameObject );
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if ( m_cardsRemaining == 0 )
        {
    
            LevelComplete();

        }
    }

    private void Update()
    {
        if (DiamondSelected)
        {
       
            DiamondSelected = false;
            Analytics.CustomEvent("DiamondSelected", new Dictionary<string, object>
        {

            { "DiamondSelected2", DiamondsSelected },
        });
        }

        levelTime += Time.deltaTime;
        if (Input.GetMouseButtonDown(0))
        {
            MouseClicksAmount++;
            print(MouseClicksAmount);
        }
    }
    public void OnMouseClick()
    {
        Analytics.CustomEvent("MouseClick", new Dictionary<string, object>
        {
            { "MouseClicksAmount", MouseClicksAmount },
        });
    }
}
